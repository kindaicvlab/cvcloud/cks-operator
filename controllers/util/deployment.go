/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package util

import (
	"strings"

	"github.com/imdario/mergo"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/utils/pointer"

	cksv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	"gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
)

func GenerateIDEDeployment(app cksv1beta2.MachineLearningApps, latestNfsPvcName, cephFsPvcName string, deploy *appsv1.Deployment) error {
	userName := app.Namespace
	appType := app.MLAppSpec.Type

	// set labels
	commonLabels := getIDECommonLabels(appType.String(), userName)
	imperatorLabels := getImperatorLabels(app.MLAppSpec.Machine, appType.String())

	labels := make(map[string]string)
	if err := mergo.Merge(&labels, commonLabels, mergo.WithOverride); err != nil {
		return err
	}
	if err := mergo.Merge(&labels, imperatorLabels, mergo.WithOverride); err != nil {
		return err
	}

	// set metadata.labels
	if deploy.ObjectMeta.Labels == nil {
		deploy.ObjectMeta.Labels = make(map[string]string)
	}
	deploy.ObjectMeta.Labels = labels

	// set .spec
	deploy.Spec = appsv1.DeploymentSpec{
		Strategy: appsv1.DeploymentStrategy{
			Type: appsv1.RecreateDeploymentStrategyType,
		},
	}

	// set replicas
	deploy.Spec.Replicas = pointer.Int32(1)

	if deploy.Spec.Selector == nil {
		deploy.Spec.Selector = &metav1.LabelSelector{
			MatchLabels: labels,
		}
	}

	// set spec.template.metadata
	if deploy.Spec.Template.ObjectMeta.Labels == nil {
		deploy.Spec.Template.ObjectMeta.Labels = make(map[string]string)
	}
	deploy.Spec.Template.ObjectMeta.Labels = labels

	// set pod security context
	deploy.Spec.Template.Spec.SecurityContext = &corev1.PodSecurityContext{
		RunAsUser:    pointer.Int64(consts.RunAsUser),
		RunAsNonRoot: pointer.Bool(true),
	}

	// set container some base information
	containers := []corev1.Container{
		{
			Name:            appType.String(),
			Image:           app.MLAppSpec.AppImage,
			ImagePullPolicy: corev1.PullAlways,
			Ports: []corev1.ContainerPort{
				{
					ContainerPort: 8888,
					Protocol:      corev1.ProtocolTCP,
				},
			},
		},
	}
	deploy.Spec.Template.Spec.Containers = containers

	// set ENV Var "[JUPYTERLAB | CODE-SERVER]_PASS"
	deploy.Spec.Template.Spec.Containers[0].Env = setDeploymentEnvVars(app)

	// set command
	deploy.Spec.Template.Spec.Containers[0].Command = setLaunchAppCommand(app)

	// set volumeMounts
	deploy.Spec.Template.Spec.Containers[0].VolumeMounts = []corev1.VolumeMount{
		{
			Name:      "dshm",
			MountPath: "/dev/shm",
		},
		{
			Name:      cephFsPvcName,
			MountPath: strings.Join([]string{"/", cephFsPvcName}, ""),
		},
		{
			Name:      latestNfsPvcName,
			MountPath: "/nas",
		},
	}

	deploy.Spec.Template.Spec.Volumes = []corev1.Volume{
		{
			Name: "dshm",
			VolumeSource: corev1.VolumeSource{
				EmptyDir: &corev1.EmptyDirVolumeSource{
					Medium: corev1.StorageMediumMemory,
				},
			},
		},
		{
			Name: cephFsPvcName,
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
					ClaimName: cephFsPvcName,
					ReadOnly:  false,
				},
			},
		},
		{
			Name: latestNfsPvcName,
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
					ClaimName: latestNfsPvcName,
					ReadOnly:  false,
				},
			},
		},
	}

	if app.MLAppSpec.ImagePullSecrets != nil {
		for _, secret := range app.MLAppSpec.ImagePullSecrets {
			if secret == "" {
				continue
			}
			deploy.Spec.Template.Spec.ImagePullSecrets = append(
				deploy.Spec.Template.Spec.ImagePullSecrets,
				corev1.LocalObjectReference{Name: secret},
			)
		}
	}

	return nil
}

func setDeploymentEnvVars(app cksv1beta2.MachineLearningApps) []corev1.EnvVar {

	appType := app.MLAppSpec.Type

	//set ENV Var "[JUPYTERLAB | CODESERVER]_PASS"
	appPasswordEnvVar := corev1.EnvVar{
		Name: strings.Join([]string{strings.ToUpper(appType.String()), "PASS"}, "_"),
		ValueFrom: &corev1.EnvVarSource{
			SecretKeyRef: &corev1.SecretKeySelector{
				Key: strings.Join([]string{appType.String(), "pass"}, "-"),
				LocalObjectReference: corev1.LocalObjectReference{
					Name: app.AppName,
				},
			},
		},
	}

	// set ENV Var "UID"
	appUIDEnvVar := corev1.EnvVar{
		Name: "UID",
		ValueFrom: &corev1.EnvVarSource{
			ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
				Key: strings.Join([]string{appType.String(), "user", "id"}, "-"),
				LocalObjectReference: corev1.LocalObjectReference{
					Name: app.AppName,
				},
			},
		},
	}

	// set ENV Var "HOME_DIR"
	homeDirEnvVar := corev1.EnvVar{
		Name: "HOME_DIR",
		ValueFrom: &corev1.EnvVarSource{
			ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
				Key: strings.Join([]string{appType.String(), "home-dir"}, "-"),
				LocalObjectReference: corev1.LocalObjectReference{
					Name: app.AppName,
				},
			},
		},
	}

	envVars := []corev1.EnvVar{
		appPasswordEnvVar,
		appUIDEnvVar,
		homeDirEnvVar,
	}

	// set ENV Var for JupyterLab
	if appType == cksv1beta2.JupyterLab {

		// set ENV Var "BASE_URL"
		baseURLEnvVar := corev1.EnvVar{
			Name: "BASE_URL",
			ValueFrom: &corev1.EnvVarSource{
				ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
					Key: strings.Join([]string{appType.String(), "base", "url"}, "-"),
					LocalObjectReference: corev1.LocalObjectReference{
						Name: app.AppName,
					},
				},
			},
		}

		envVars = append(envVars, baseURLEnvVar)

	}

	return envVars
}

func setLaunchAppCommand(app cksv1beta2.MachineLearningApps) []string {

	appType := app.MLAppSpec.Type
	command := []string{"sh", "-c"}

	jupyterlabCommand := strings.Join([]string{
		"PASSWORD=$(python -c 'from notebook.auth import passwd;print(passwd(\"'${JUPYTERLAB_PASS}'\"))')",
		"jupyter-lab --port=8888 --ip=0.0.0.0 --no-browser --NotebookApp.token='' --NotebookApp.base_url=${BASE_URL} --NotebookApp.password=${PASSWORD} --NotebookApp.notebook_dir=${HOME_DIR}",
	}, "\n")

	codeServerCommand := strings.Join([]string{
		"CODESERVER_SETTINGS_DIR=/home/user/.local/share/code-server/User",
		"cat ${CODESERVER_SETTINGS_DIR}/settings.json > ${CODESERVER_SETTINGS_DIR}/cache.json",
		"cat ${CODESERVER_SETTINGS_DIR}/cvcloud/controller.json | sed \"s|@@HOME_DIR@@|${HOME_DIR}|\" > ${CODESERVER_SETTINGS_DIR}/add.json",
		"jq -s add ${CODESERVER_SETTINGS_DIR}/cache.json ${CODESERVER_SETTINGS_DIR}/add.json > ${CODESERVER_SETTINGS_DIR}/settings.json",
		"PASSWORD=${CODESERVER_PASS} dumb-init /usr/bin/code-server --bind-addr 0.0.0.0:8888 --auth password --cert false",
	}, "\n")

	switch appType {
	case cksv1beta2.JupyterLab:
		command = append(command, jupyterlabCommand)
	case cksv1beta2.CodeServer:
		command = append(command, codeServerCommand)
	}
	return command
}
