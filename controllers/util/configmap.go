/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package util

import (
	corev1 "k8s.io/api/core/v1"

	cksv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
)

func GenerateConfigMap(app cksv1beta2.MachineLearningApps, config *corev1.ConfigMap) {
	userName := app.Namespace
	appType := app.MLAppSpec.Type

	// set labels
	labels := getIDECommonLabels(appType.String(), userName)

	// set metadata.labels
	if config.ObjectMeta.Labels == nil {
		config.ObjectMeta.Labels = make(map[string]string)
	}
	config.ObjectMeta.Labels = labels

	// determine env vars
	envVars := make(map[string]string)
	if appType == cksv1beta2.JupyterLab {
		envVars = map[string]string{
			cksv1beta2.JupyterLab.String() + "-user-name": userName,
			cksv1beta2.JupyterLab.String() + "-user-id":   "1000",
			cksv1beta2.JupyterLab.String() + "-base-url":  "/" + userName + "/",
			cksv1beta2.JupyterLab.String() + "-home-dir":  "/" + userName + "-pvc",
			"nfs-data-dir": "/nas/data",
		}
	} else if appType == cksv1beta2.CodeServer {
		envVars = map[string]string{
			cksv1beta2.CodeServer.String() + "-user-name": userName,
			cksv1beta2.CodeServer.String() + "-user-id":   "1000",
			cksv1beta2.CodeServer.String() + "-home-dir":  "/" + userName + "-pvc",
			"nfs-data-dir": "/nfs/data",
		}
	}

	// set data
	if config.Data == nil {
		config.Data = make(map[string]string)
	}
	config.Data = envVars
}
