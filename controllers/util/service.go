/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package util

import (
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/util/intstr"

	cksv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
)

func GenerateIDEService(app cksv1beta2.MachineLearningApps, service *corev1.Service) {

	appType := app.MLAppSpec.Type
	userName := app.Namespace

	// set labels
	labels := getIDECommonLabels(appType.String(), userName)

	//set metdata.labels
	if service.ObjectMeta.Labels == nil {
		service.ObjectMeta.Labels = make(map[string]string)
	}
	service.ObjectMeta.Labels = labels

	// set .spec.type
	service.Spec.Type = corev1.ServiceTypeClusterIP

	// set spec.selector
	if service.Spec.Selector == nil {
		service.Spec.Selector = make(map[string]string)
	}
	service.Spec.Selector = labels

	// set spec.ports
	service.Spec.Ports = []corev1.ServicePort{
		{
			Protocol: corev1.ProtocolTCP,
			Port:     80,
			TargetPort: intstr.IntOrString{
				IntVal: 8888,
			},
		},
	}
}
