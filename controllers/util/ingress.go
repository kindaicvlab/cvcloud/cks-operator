/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package util

import (
	networkv1 "k8s.io/api/networking/v1"
	"k8s.io/utils/pointer"

	cksv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	"gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
)

func GenerateIDEIngress(app cksv1beta2.MachineLearningApps, ingress *networkv1.Ingress) {

	userName := app.Namespace
	appType := app.MLAppSpec.Type

	// set labels
	labels := getIDECommonLabels(appType.String(), userName)

	if ingress.ObjectMeta.Labels == nil {
		ingress.ObjectMeta.Labels = make(map[string]string)
	}
	ingress.ObjectMeta.Labels = labels

	//set annotations
	annotations := make(map[string]string)
	switch appType {
	case cksv1beta2.JupyterLab:
		annotations = map[string]string{
			"ingress.kubernetes.io/secure-backends": "true",
		}
	case cksv1beta2.CodeServer:
		annotations = map[string]string{
			"ingress.kubernetes.io/secure-backends":      "true",
			"nginx.ingress.kubernetes.io/rewrite-target": "/$2",
			"ngnix.ingress.kubernetes.io/use-regex":      "true",
		}
	}

	if ingress.ObjectMeta.Annotations == nil {
		ingress.ObjectMeta.Annotations = make(map[string]string)
	}
	ingress.ObjectMeta.Annotations = annotations

	// set spec.ingressClassName
	ingress.Spec.IngressClassName = pointer.String(consts.IngressClassName)

	// set spec.rules
	if ingress.Spec.Rules == nil {
		ingress.Spec.Rules = []networkv1.IngressRule{}
	}

	pathTypeImplementationSpecific := networkv1.PathTypeImplementationSpecific
	ingress.Spec.Rules = []networkv1.IngressRule{
		{
			Host: consts.IngressHostName,
			IngressRuleValue: networkv1.IngressRuleValue{
				HTTP: &networkv1.HTTPIngressRuleValue{
					Paths: []networkv1.HTTPIngressPath{
						{
							PathType: &pathTypeImplementationSpecific,
							Backend: networkv1.IngressBackend{
								Service: &networkv1.IngressServiceBackend{
									Name: app.AppName,
									Port: networkv1.ServiceBackendPort{},
								},
							},
						},
					},
				},
			},
		},
	}

	// set spec.rules.http.paths.path
	switch appType {
	case cksv1beta2.JupyterLab:
		ingress.Spec.Rules[0].HTTP.Paths[0].Path = "/" + userName
	case cksv1beta2.CodeServer:
		ingress.Spec.Rules[0].HTTP.Paths[0].Path = "/" + userName + "(/|$)(.*)"
	}

	// set spec.rules.http.paths.backend.service.port
	ingress.Spec.Rules[0].HTTP.Paths[0].Backend.Service.Port.Number = 80

}
