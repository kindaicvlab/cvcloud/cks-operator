/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package util

import (
	"math/big"

	"crypto/rand"
	corev1 "k8s.io/api/core/v1"

	cksv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
)

func GenerateSecret(app cksv1beta2.MachineLearningApps, secret *corev1.Secret) {

	userName := app.Namespace
	appType := app.MLAppSpec.Type

	// set labels
	labels := getIDECommonLabels(appType.String(), userName)

	// set metadata.labels
	if secret.ObjectMeta.Labels == nil {
		secret.ObjectMeta.Labels = make(map[string]string)
	}
	secret.ObjectMeta.Labels = labels

	// set .type
	secret.Type = corev1.SecretTypeOpaque

	// set .data
	if secret.Data == nil {
		n, _ := rand.Int(rand.Reader, big.NewInt(1000))
		appPassword := []byte(userName + n.String())
		secret.Data = map[string][]byte{
			appType.String() + "-pass": appPassword,
		}
	}
}
