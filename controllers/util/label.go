/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package util

import (
	"strings"

	imperatorv1alpha1consts "github.com/tenzen-y/imperator/pkg/consts"

	cksv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	"gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
)

func getIDECommonLabels(appType, userName string) map[string]string {
	return map[string]string{
		consts.CksAppTypeLabelKey: appType,
		consts.CksAppNameLabelKey: strings.Join([]string{appType, userName}, "-"),
	}
}

func getImperatorLabels(machineSpec cksv1beta2.MachineSpec, primaryContainerName string) map[string]string {
	return map[string]string{
		imperatorv1alpha1consts.MachineTypeKey:                          machineSpec.Type,
		imperatorv1alpha1consts.MachineGroupKey:                         machineSpec.Group,
		imperatorv1alpha1consts.PodRoleKey:                              imperatorv1alpha1consts.PodRoleGuest,
		imperatorv1alpha1consts.ImperatorResourceInjectContainerNameKey: primaryContainerName,
	}
}
