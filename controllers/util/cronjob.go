/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package util

import (
	"strings"

	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"k8s.io/utils/pointer"

	cksv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	"gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
)

func GenerateDiffBackupCronJob(app cksv1beta2.MachineLearningApps, latestNfsPvcName, cephFsPvcName string, cronJob *batchv1.CronJob) {
	userName := app.Namespace
	labels := getIDECommonLabels(consts.DiffBackup, userName)

	// set metadata.labels
	if cronJob.ObjectMeta.Labels == nil {
		cronJob.ObjectMeta.Labels = make(map[string]string)
	}
	cronJob.ObjectMeta.Labels = labels

	// set base settings
	cronJob.Spec = batchv1.CronJobSpec{
		Schedule:                   "*/5 * * * *",
		ConcurrencyPolicy:          batchv1.ForbidConcurrent,
		StartingDeadlineSeconds:    pointer.Int64(300),
		SuccessfulJobsHistoryLimit: pointer.Int32(5),
		FailedJobsHistoryLimit:     pointer.Int32(5),
		Suspend:                    pointer.Bool(false),
	}

	// set spec.jobTemplate.spec
	cronJob.Spec.JobTemplate.Spec = batchv1.JobSpec{
		Completions:  pointer.Int32(1),
		Parallelism:  pointer.Int32(1),
		BackoffLimit: pointer.Int32(0),
	}

	// set job labels
	if cronJob.Spec.JobTemplate.Spec.Template.ObjectMeta.Labels == nil {
		cronJob.Spec.JobTemplate.Spec.Template.ObjectMeta.Labels = make(map[string]string)
	}
	cronJob.Spec.JobTemplate.Spec.Template.ObjectMeta.Labels = labels

	// set affinity
	cronJob.Spec.JobTemplate.Spec.Template.Spec.Affinity = &corev1.Affinity{
		NodeAffinity: &corev1.NodeAffinity{
			PreferredDuringSchedulingIgnoredDuringExecution: []corev1.PreferredSchedulingTerm{
				{
					Weight: 100,
					Preference: corev1.NodeSelectorTerm{
						MatchExpressions: []corev1.NodeSelectorRequirement{
							{
								Key:      "hardware-type",
								Operator: corev1.NodeSelectorOpIn,
								Values: []string{
									"NonGPU",
								},
							},
						},
					},
				},
			},
		},
	}

	// set security context
	if cronJob.Spec.JobTemplate.Spec.Template.Spec.SecurityContext == nil {
		cronJob.Spec.JobTemplate.Spec.Template.Spec.SecurityContext = &corev1.PodSecurityContext{
			RunAsUser:    pointer.Int64(consts.RunAsUser),
			RunAsNonRoot: pointer.Bool(true),
		}
	}

	rmCommand := strings.Join([]string{
		"mkdir -p ${DEST_BASE_DIR}",
		"mkdir -p ${DEST_DIFF_DIR}",
		"mkdir -p ${NFS_DATA_DIR}",
		"DIRS_NUM=$(ls ${DEST_DIFF_DIR} | wc -w)",
		"RM_DIRS=$(ls -d ${DEST_DIFF_DIR}* | xargs readlink -f | head -n $((${DIRS_NUM}-${KEEP_DIRS_NUM})))",
		"if [ ${DIRS_NUM} -gt ${KEEP_DIRS_NUM} ];then rm -vr ${RM_DIRS};fi",
	}, "\n")

	backupContainerResourceList := corev1.ResourceRequirements{
		Limits: corev1.ResourceList{
			corev1.ResourceCPU:    resource.MustParse("2000m"),
			corev1.ResourceMemory: resource.MustParse("16Gi"),
		},
		Requests: corev1.ResourceList{
			corev1.ResourceCPU:    resource.MustParse("1000m"),
			corev1.ResourceMemory: resource.MustParse("8Gi"),
		},
	}

	cronJob.Spec.JobTemplate.Spec.Template.Spec.InitContainers = []corev1.Container{
		{
			Name:  "rm-container",
			Image: consts.BackUpImage,
			Env: []corev1.EnvVar{
				{
					Name: "DEST_BASE_DIR",
					ValueFrom: &corev1.EnvVarSource{
						ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
							Key: "dest-base-dir",
							LocalObjectReference: corev1.LocalObjectReference{
								Name: consts.BackUpConfigMapName,
							},
						},
					},
				},
				{
					Name: "DEST_DIFF_DIR",
					ValueFrom: &corev1.EnvVarSource{
						ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
							Key: "dest-diff-dir",
							LocalObjectReference: corev1.LocalObjectReference{
								Name: consts.BackUpConfigMapName,
							},
						},
					},
				},
				{
					Name: "KEEP_DIRS_NUM",
					ValueFrom: &corev1.EnvVarSource{
						ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
							Key: "keep-dirs-num",
							LocalObjectReference: corev1.LocalObjectReference{
								Name: consts.BackUpConfigMapName,
							},
						},
					},
				},
				{
					Name: "NFS_DATA_DIR",
					ValueFrom: &corev1.EnvVarSource{
						ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
							Key: "nfs-data-dir",
							LocalObjectReference: corev1.LocalObjectReference{
								Name: app.AppName,
							},
						},
					},
				},
			},
			Command: []string{
				"sh",
				"-c",
				rmCommand,
			},
			VolumeMounts: []corev1.VolumeMount{
				{
					Name:      cephFsPvcName,
					MountPath: strings.Join([]string{"/", userName, "-pvc"}, ""),
				},
				{
					Name:      latestNfsPvcName,
					MountPath: "/nas",
				},
			},
			Resources: backupContainerResourceList,
		},
	}

	backUpCommand := strings.Join([]string{
		"DEST_FULL_FILE=$(date \"+%Y%m%d-%H%M%S\")",
		"LINK_FULL_FILE=$(ls ${LINK_BASE_DIR} | tail -n 1)",
		"rsync -rlOtcv --delete --link-dest=${LINK_BASE_DIR}${LINK_FULL_FILE} ${ORIGINAL_DIR} ${DEST_DIFF_DIR}${DEST_FULL_FILE}",
	}, "\n")

	cronJob.Spec.JobTemplate.Spec.Template.Spec.Containers = []corev1.Container{
		{
			Name:  "backup-container",
			Image: consts.BackUpImage,
			Env: []corev1.EnvVar{
				{
					Name: "LINK_BASE_DIR",
					ValueFrom: &corev1.EnvVarSource{
						ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
							Key: "dest-base-dir",
							LocalObjectReference: corev1.LocalObjectReference{
								Name: consts.BackUpConfigMapName,
							},
						},
					},
				},
				{
					Name: "ORIGINAL_DIR",
					ValueFrom: &corev1.EnvVarSource{
						ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
							Key: "original-dir",
							LocalObjectReference: corev1.LocalObjectReference{
								Name: consts.BackUpConfigMapName,
							},
						},
					},
				},
				{
					Name: "DEST_DIFF_DIR",
					ValueFrom: &corev1.EnvVarSource{
						ConfigMapKeyRef: &corev1.ConfigMapKeySelector{
							Key: "dest-diff-dir",
							LocalObjectReference: corev1.LocalObjectReference{
								Name: consts.BackUpConfigMapName,
							},
						},
					},
				},
			},
			Command: []string{
				"sh",
				"-c",
				backUpCommand,
			},
			VolumeMounts: []corev1.VolumeMount{
				{
					Name:      cephFsPvcName,
					MountPath: strings.Join([]string{"/", userName, "-pvc"}, ""),
				},
				{
					Name:      latestNfsPvcName,
					MountPath: "/nas",
				},
			},
			Resources: backupContainerResourceList,
		},
	}

	cronJob.Spec.JobTemplate.Spec.Template.Spec.Volumes = []corev1.Volume{
		{
			Name: cephFsPvcName,
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
					ClaimName: cephFsPvcName,
					ReadOnly:  false,
				},
			},
		},
		{
			Name: latestNfsPvcName,
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
					ClaimName: latestNfsPvcName,
					ReadOnly:  false,
				},
			},
		},
	}

	cronJob.Spec.JobTemplate.Spec.Template.Spec.RestartPolicy = corev1.RestartPolicyNever
}
