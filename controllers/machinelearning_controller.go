/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"strings"

	"github.com/go-logr/logr"
	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	networkv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"

	cksv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	"gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
	"gitlab.com/kindaicvlab/cvcloud/cks-operator/controllers/util"
)

// MachineLearningReconciler reconciles a MachineLearning object
type MachineLearningReconciler struct {
	client.Client
	Scheme   *runtime.Scheme
	Recorder record.EventRecorder
}

// +kubebuilder:rbac:groups=apps.cks.cvlab,resources=machinelearnings,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps.cks.cvlab,resources=machinelearnings/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=apps.cks.cvlab,resources=machinelearnings/finalizers,verbs=update
// +kubebuilder:rbac:groups=imperator.tenzen-y.io,resources=machines,verbs=get;list;watch
// +kubebuilder:rbac:groups=imperator.tenzen-y.io,resources=machines/status,verbs=get;list;watch
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=batch,resources=cronjobs,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=networking.k8s.io,resources=ingresses, verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=namespaces,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=configmaps,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=secrets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=events,verbs=create;patch

// Reconcile is main function for reconciliation loop
func (r *MachineLearningReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	machineLearning := &cksv1beta2.MachineLearning{}
	logger.Info("fetching MachineLearning Resource")
	if err := r.Get(ctx, req.NamespacedName, machineLearning); err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	// Start To Reconcile
	return r.reconcile(ctx, machineLearning)
}

func (r *MachineLearningReconciler) reconcile(ctx context.Context, machineLearning *cksv1beta2.MachineLearning) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	if err := r.reconcileConfigmap(ctx, machineLearning); err != nil {
		logger.Error(err, "failed to reconcile ConfigMap", "name", machineLearning.Name)
		return r.updateReconcileFailedStatus(ctx, machineLearning, err)
	}

	if err := r.reconcileSecret(ctx, machineLearning); err != nil {
		logger.Error(err, "failed to reconcile Secret", "name", machineLearning.Name)
		return r.updateReconcileFailedStatus(ctx, machineLearning, err)
	}

	if err := r.reconcileService(ctx, machineLearning); err != nil {
		logger.Error(err, "failed to reconcile Service", "name", machineLearning.Name)
		return r.updateReconcileFailedStatus(ctx, machineLearning, err)
	}

	if err := r.reconcileIngress(ctx, machineLearning); err != nil {
		logger.Error(err, "failed to reconcile Ingress", "name", machineLearning.Name)
		return r.updateReconcileFailedStatus(ctx, machineLearning, err)
	}

	if err := r.reconcileDeployment(ctx, machineLearning); err != nil {
		logger.Error(err, "failed to reconcile Deployment", "name", machineLearning.Name)
		return r.updateReconcileFailedStatus(ctx, machineLearning, err)
	}

	if err := r.reconcileCronJob(ctx, machineLearning); err != nil {
		logger.Error(err, "failed to reconcile CronJob", "name", machineLearning.Name)
		return r.updateReconcileFailedStatus(ctx, machineLearning, err)
	}

	return r.updateStatus(ctx, machineLearning)
}

func (r *MachineLearningReconciler) updateReconcileFailedStatus(ctx context.Context, machineLearning *cksv1beta2.MachineLearning, reconcileErr error) (ctrl.Result, error) {
	r.Recorder.Eventf(machineLearning, corev1.EventTypeWarning, "Failed",
		"failed to reconcile: %s", reconcileErr.Error())
	meta.SetStatusCondition(&machineLearning.Status.Conditions, metav1.Condition{
		Type:               consts.ConditionReady,
		Status:             metav1.ConditionFalse,
		LastTransitionTime: metav1.Now(),
		Reason:             metav1.StatusFailure,
		Message:            reconcileErr.Error(),
	})
	if err := r.Status().Update(ctx, machineLearning, &client.UpdateOptions{}); err != nil {
		return ctrl.Result{Requeue: true}, err
	}
	return ctrl.Result{}, nil
}

func (r *MachineLearningReconciler) updateStatus(ctx context.Context, machineLearning *cksv1beta2.MachineLearning) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	r.Recorder.Eventf(machineLearning, corev1.EventTypeNormal, "Updated",
		"the MachineLearning was updated")

	meta.SetStatusCondition(&machineLearning.Status.Conditions, metav1.Condition{
		Type:               consts.ConditionReady,
		Status:             metav1.ConditionTrue,
		LastTransitionTime: metav1.Now(),
		Reason:             metav1.StatusSuccess,
		Message:            "updated",
	})
	if err := r.Status().Update(ctx, machineLearning); err != nil {
		logger.Error(err, "failed to update status.conditions", "name", machineLearning.Name)
		return ctrl.Result{}, err
	}
	return ctrl.Result{}, nil
}

func (r *MachineLearningReconciler) reconcileDeployment(ctx context.Context, machineLearning *cksv1beta2.MachineLearning) error {

	logger := log.FromContext(ctx)

	for _, app := range machineLearning.Spec.MachineLearningApps {
		dep := &appsv1.Deployment{
			TypeMeta: metav1.TypeMeta{
				APIVersion: appsv1.SchemeGroupVersion.String(),
				Kind:       "Deployment",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      app.AppName,
				Namespace: app.Namespace,
			},
		}
		opeResult, err := ctrl.CreateOrUpdate(ctx, r.Client, dep, func() error {
			latestNfsPvc, err := r.GetLatestPVCByNFSProvisioner(ctx, app.Namespace)
			if err != nil {
				logger.Error(err, "your nfs storage is not found")
				return err
			}
			cephFsPvc, err := r.GetCephFSPVC(ctx, app.Namespace)
			if err != nil {
				logger.Error(err, "your cephfs storage is not found")
				return err
			}
			if err = util.GenerateIDEDeployment(app, latestNfsPvc.Name, cephFsPvc.Name, dep); err != nil {
				return err
			}
			return ctrl.SetControllerReference(machineLearning, dep, r.Scheme)
		})

		if err != nil {
			return fmt.Errorf("failed to reconcile Deployment for %s; %v", app.AppName, err)
		}
		if opeResult == controllerutil.OperationResultNone {
			logger.Info(fmt.Sprintf("reconciled Deployment for %s", app.AppName))
		}
		if opeResult == controllerutil.OperationResultCreated {
			continue
		}
		if opeResult == controllerutil.OperationResultUpdated {
			logger.Info(fmt.Sprintf("updated Deployment for %s", app.AppName))
		}

	}
	return nil
}

func (r *MachineLearningReconciler) reconcileIngress(ctx context.Context, machineLearning *cksv1beta2.MachineLearning) error {

	logger := log.FromContext(ctx)

	for _, app := range machineLearning.Spec.MachineLearningApps {

		ing := &networkv1.Ingress{
			TypeMeta: metav1.TypeMeta{
				APIVersion: networkv1.SchemeGroupVersion.String(),
				Kind:       "Ingress",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      app.AppName,
				Namespace: app.Namespace,
			},
		}

		opeResult, err := ctrl.CreateOrUpdate(ctx, r.Client, ing, func() error {
			util.GenerateIDEIngress(app, ing)
			return ctrl.SetControllerReference(machineLearning, ing, r.Scheme)
		})

		if err != nil {
			return fmt.Errorf("failed to reconcile Ingress for %s; %v", app.AppName, err)
		}
		if opeResult == controllerutil.OperationResultNone {
			logger.Info(fmt.Sprintf("reconciled Ingress for %s", app.AppName))
		}
		if opeResult == controllerutil.OperationResultCreated {
			continue
		}
		if opeResult == controllerutil.OperationResultUpdated {
			logger.Info(fmt.Sprintf("updated Ingress for %s", app.AppName))
		}

	}
	return nil
}

func (r *MachineLearningReconciler) reconcileService(ctx context.Context, machineLearning *cksv1beta2.MachineLearning) error {
	logger := logr.FromContext(ctx)

	for _, app := range machineLearning.Spec.MachineLearningApps {

		svc := &corev1.Service{
			TypeMeta: metav1.TypeMeta{
				APIVersion: corev1.SchemeGroupVersion.String(),
				Kind:       "Service",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      app.AppName,
				Namespace: app.Namespace,
			},
		}

		opeResult, err := ctrl.CreateOrUpdate(ctx, r.Client, svc, func() error {
			util.GenerateIDEService(app, svc)
			return ctrl.SetControllerReference(machineLearning, svc, r.Scheme)
		})

		if err != nil {
			return fmt.Errorf("failed to reconcile Service for %s; %v", app.AppName, err)
		}
		if opeResult == controllerutil.OperationResultNone {
			logger.Info(fmt.Sprintf("reconciled Service for %s", app.AppName))
		}
		if opeResult == controllerutil.OperationResultCreated {
			continue
		}
		if opeResult == controllerutil.OperationResultUpdated {
			logger.Info(fmt.Sprintf("updated Service for %s", app.AppName))
		}

	}
	return nil
}

func (r *MachineLearningReconciler) reconcileCronJob(ctx context.Context, machineLearning *cksv1beta2.MachineLearning) error {

	logger := log.FromContext(ctx)

	for _, app := range machineLearning.Spec.MachineLearningApps {
		cj := &batchv1.CronJob{
			TypeMeta: metav1.TypeMeta{
				APIVersion: batchv1.SchemeGroupVersion.String(),
				Kind:       "CronJob",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      strings.Join([]string{app.AppName, consts.DiffBackup}, "-"),
				Namespace: app.Namespace,
			},
		}
		opeResult, err := ctrl.CreateOrUpdate(ctx, r.Client, cj, func() error {
			latestNfsPvc, err := r.GetLatestPVCByNFSProvisioner(ctx, app.Namespace)
			if err != nil {
				logger.Error(err, "your nfs storage is not found")
				return err
			}
			cephFsPvc, err := r.GetCephFSPVC(ctx, app.Namespace)
			if err != nil {
				logger.Error(err, "your cephfs storage is not found")
				return err
			}
			util.GenerateDiffBackupCronJob(app, latestNfsPvc.Name, cephFsPvc.Name, cj)
			return ctrl.SetControllerReference(machineLearning, cj, r.Scheme)
		})

		if err != nil {
			return fmt.Errorf("failed to reconcile CronJob for %s; %v", app.AppName, err)
		}
		if opeResult == controllerutil.OperationResultNone {
			logger.Info(fmt.Sprintf("reconciled CronJob for %s", app.AppName))
		}
		if opeResult == controllerutil.OperationResultCreated {
			continue
		}
		if opeResult == controllerutil.OperationResultUpdated {
			logger.Info(fmt.Sprintf("updated CronJob for %s", app.AppName))
		}

	}
	return nil
}

func (r *MachineLearningReconciler) reconcileConfigmap(ctx context.Context, machineLearning *cksv1beta2.MachineLearning) error {

	logger := log.FromContext(ctx)

	for _, app := range machineLearning.Spec.MachineLearningApps {
		cm := &corev1.ConfigMap{
			TypeMeta: metav1.TypeMeta{
				APIVersion: corev1.SchemeGroupVersion.String(),
				Kind:       "ConfigMap",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      app.AppName,
				Namespace: app.Namespace,
			},
		}
		opeResult, err := ctrl.CreateOrUpdate(ctx, r.Client, cm, func() error {
			util.GenerateConfigMap(app, cm)
			return ctrl.SetControllerReference(machineLearning, cm, r.Scheme)
		})

		if err != nil {
			return fmt.Errorf("failed to reconcile ConfigMap for %s; %v", app.AppName, err)
		}
		if opeResult == controllerutil.OperationResultNone {
			logger.Info(fmt.Sprintf("reconciled ConfigMap for %s", app.AppName))
		}
		if opeResult == controllerutil.OperationResultCreated {
			continue
		}
		if opeResult == controllerutil.OperationResultUpdated {
			logger.Info(fmt.Sprintf("updated ConfigMap for %s", app.AppName))
		}
	}
	return nil
}

func (r *MachineLearningReconciler) reconcileSecret(ctx context.Context, machineLearning *cksv1beta2.MachineLearning) error {

	logger := log.FromContext(ctx)

	for _, app := range machineLearning.Spec.MachineLearningApps {

		secret := &corev1.Secret{
			TypeMeta: metav1.TypeMeta{
				APIVersion: corev1.SchemeGroupVersion.String(),
				Kind:       "Secret",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      app.AppName,
				Namespace: app.Namespace,
			},
		}

		opeResult, err := ctrl.CreateOrUpdate(ctx, r.Client, secret, func() error {
			util.GenerateSecret(app, secret)
			return ctrl.SetControllerReference(machineLearning, secret, r.Scheme)
		})

		if err != nil {
			return fmt.Errorf("failed to reconcile Secret for %s; %v", app.AppName, err)
		}
		if opeResult == controllerutil.OperationResultNone {
			logger.Info(fmt.Sprintf("reconciled Secret for %s", app.AppName))
		}
		if opeResult == controllerutil.OperationResultCreated {
			continue
		}
		if opeResult == controllerutil.OperationResultUpdated {
			logger.Info(fmt.Sprintf("updated Secret for %s", app.AppName))
		}
	}
	return nil
}

// SetupWithManager set up reconciliation loop
func (r *MachineLearningReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&cksv1beta2.MachineLearning{}).
		Owns(&corev1.ConfigMap{}).
		Owns(&corev1.Secret{}).
		Owns(&corev1.Service{}).
		Owns(&networkv1.Ingress{}).
		Owns(&appsv1.Deployment{}).
		Owns(&batchv1.CronJob{}).
		Complete(r)
}
