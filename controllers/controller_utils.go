/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/labels"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sort"
	"strconv"
	"strings"
	"sync"
)

// PVCDetailInfo is pvc name's detail to provide nfs storages
type PVCDetailInfo struct {
	Name        string
	StatusPhase string
	CreatedYear int
	CreatedDay  int
	CreatedTime int
}

func (r *MachineLearningReconciler) getPVC(ctx context.Context, pvcs *corev1.PersistentVolumeClaimList, listOpts *client.ListOptions) error {
	if err := r.List(ctx, pvcs, listOpts); err != nil {
		return err
	}
	return nil
}

// GetCephFSPVC is tools to search pvc for cephfs deployed by cks-operator in user k8s namespace
func (r *MachineLearningReconciler) GetCephFSPVC(ctx context.Context, ns string) (*PVCDetailInfo, error) {
	pvcs := &corev1.PersistentVolumeClaimList{}
	listOpts := &client.ListOptions{
		Namespace: ns,
		LabelSelector: labels.SelectorFromSet(map[string]string{
			consts.CksStorageTypeLabelKey: consts.CephFsLabel,
		}),
	}
	if err := r.getPVC(ctx, pvcs, listOpts); err != nil {
		return nil, err
	}
	if len(pvcs.Items) == 0 {
		return nil, fmt.Errorf("your persistetnt volume provided by CephFS is not found. Please create your persistent volume by cksctl")
	}

	cephFSPVC := &PVCDetailInfo{
		Name:        pvcs.Items[0].Name,
		StatusPhase: string(pvcs.Items[0].Status.Phase),
	}
	if len(pvcs.Items) < 2 {
		if cephFSPVC.Name == "" {
			return nil, fmt.Errorf("CephFS Name is not Found")
		}
		if cephFSPVC.StatusPhase != string(corev1.ClaimBound) {
			return nil, fmt.Errorf("CephFs status is %s", cephFSPVC.StatusPhase)
		}
	} else {
		return nil, fmt.Errorf("your persistent volume provided by CephFS is duplicated. Contact the administrator")
	}
	return cephFSPVC, nil
}

// GetLatestPVCByNFSProvisioner is tools to search latest pvc for nfs deployed by cks-operator in user k8s namespace
func (r *MachineLearningReconciler) GetLatestPVCByNFSProvisioner(ctx context.Context, ns string) (*PVCDetailInfo, error) {

	pvcs := &corev1.PersistentVolumeClaimList{}
	listOpts := &client.ListOptions{
		Namespace: ns,
		LabelSelector: labels.SelectorFromSet(map[string]string{
			consts.CksStorageTypeLabelKey: consts.NfsLabel,
		}),
	}
	if err := r.getPVC(ctx, pvcs, listOpts); err != nil {
		return nil, err
	}
	if len(pvcs.Items) == 0 {
		return nil, fmt.Errorf("your persistent volume provided by cks managed nfs storage is not found")
	}

	var pvcDetailInfo []PVCDetailInfo

	var wg sync.WaitGroup
	for _, partOfPVC := range pvcs.Items {
		wg.Add(1)

		go func(p corev1.PersistentVolumeClaim) {
			defer wg.Done()

			pvcStatusPhase := string(p.Status.Phase)
			pvcNameSlice := strings.Split(p.Name, "-")
			pvcCreatedYear, _ := strconv.Atoi(pvcNameSlice[2][0:3])
			pvcCreatedDay, _ := strconv.Atoi(pvcNameSlice[2][4:7])
			pvcCreatedTime, _ := strconv.Atoi(pvcNameSlice[3])

			pvcDetailInfo = append(pvcDetailInfo, PVCDetailInfo{
				StatusPhase: pvcStatusPhase,
				Name:        p.Name,
				CreatedYear: pvcCreatedYear,
				CreatedDay:  pvcCreatedDay,
				CreatedTime: pvcCreatedTime,
			})
		}(partOfPVC)
	}
	wg.Wait()
	sort.SliceStable(pvcDetailInfo, func(i, j int) bool { return pvcDetailInfo[i].CreatedYear > pvcDetailInfo[j].CreatedYear })
	sort.SliceStable(pvcDetailInfo, func(i, j int) bool { return pvcDetailInfo[i].CreatedDay > pvcDetailInfo[j].CreatedDay })
	sort.SliceStable(pvcDetailInfo, func(i, j int) bool { return pvcDetailInfo[i].CreatedTime > pvcDetailInfo[j].CreatedTime })

	latestNfsPvc := &pvcDetailInfo[0]
	if latestNfsPvc.Name == "" {
		return nil, fmt.Errorf("nfs pvc name is not found")
	}
	if latestNfsPvc.StatusPhase != string(corev1.ClaimBound) {
		return nil, fmt.Errorf(strings.Join([]string{"nfs pvc status is", latestNfsPvc.StatusPhase}, ": "))
	}

	return latestNfsPvc, nil
}
