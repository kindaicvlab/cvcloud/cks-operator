/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"strings"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	networkv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/utils/pointer"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	cksv1beta2 "gitlab.com/kindaicvlab/cvcloud/cks-operator/api/v1beta2"
	"gitlab.com/kindaicvlab/cvcloud/cks-operator/consts"
)

const (
	testMLName            = "machinelearning-test"
	testJupyterAppName    = "jupyterlab-test"
	testCodeServerAppName = "codeserver-test"
	testAppNs             = "test"
)

func newFakeMachineLearning() *cksv1beta2.MachineLearning {
	return &cksv1beta2.MachineLearning{
		TypeMeta: metav1.TypeMeta{
			APIVersion: cksv1beta2.GroupVersion.String(),
			Kind:       "MachineLearning",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      testMLName,
			Namespace: testAppNs,
		},
		Spec: cksv1beta2.MachineLearningSpec{
			MachineLearningApps: []cksv1beta2.MachineLearningApps{
				{
					AppName:   testJupyterAppName,
					Namespace: testAppNs,
					MLAppSpec: cksv1beta2.MLAppSpec{
						Type: cksv1beta2.JupyterLab,
						Machine: cksv1beta2.MachineSpec{
							Group: "test-machine-group",
							Type:  "test1",
						},
						AppImage: "nginx:latest",
						ImagePullSecrets: []string{
							"test-secret",
						},
					},
				},
			},
		},
	}
}

func newFakeNfsPVC() *corev1.PersistentVolumeClaim {
	volumeMode := corev1.PersistentVolumeFilesystem
	return &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{testAppNs, "nfs-20201018-0418"}, "-"),
			Namespace: testAppNs,
			Labels: map[string]string{
				consts.CksStorageTypeLabelKey: consts.NfsLabel,
			},
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteOnce,
			},
			VolumeMode: &volumeMode,
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceStorage: resource.MustParse("1Gi"),
				},
			},
			StorageClassName: pointer.String("manual"),
		},
	}
}

func newFakeCephFsPVC() *corev1.PersistentVolumeClaim {
	return &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Join([]string{testAppNs, "-pvc"}, "-"),
			Namespace: testAppNs,
			Labels: map[string]string{
				consts.CksStorageTypeLabelKey: consts.CephFsLabel,
			},
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteMany,
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceStorage: resource.MustParse("2Gi"),
				},
			},
			StorageClassName: pointer.String("manual"),
		},
	}
}

func updatePvcStatusPhase(ctx context.Context, pvc *corev1.PersistentVolumeClaim) {
	getPVC := &corev1.PersistentVolumeClaim{}
	Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(pvc), getPVC)).NotTo(HaveOccurred())
	getPVC.Status.Phase = corev1.ClaimBound
	Eventually(func() error {
		return k8sClient.Status().Update(ctx, getPVC, &client.UpdateOptions{})
	}).Should(BeNil())
}

func checkOwnerReference(obj client.Object, expected metav1.OwnerReference) {
	Expect(obj.GetOwnerReferences()).To(ContainElement(expected))
}

var _ = Describe("cks operator", func() {
	ctx := context.Background()
	var cancelFunc context.CancelFunc
	testJupyterClientObjectKey := client.ObjectKey{Name: testJupyterAppName, Namespace: testAppNs}
	testCodeServerClientObjectKey := client.ObjectKey{Name: testCodeServerAppName, Namespace: testAppNs}

	BeforeEach(func() {
		// clean up env
		listOpts := client.InNamespace(testAppNs)

		Expect(k8sClient.DeleteAllOf(ctx, &appsv1.Deployment{}, listOpts)).NotTo(HaveOccurred())
		Expect(k8sClient.DeleteAllOf(ctx, &corev1.ConfigMap{}, listOpts)).NotTo(HaveOccurred())
		Expect(k8sClient.DeleteAllOf(ctx, &batchv1.CronJob{}, listOpts)).NotTo(HaveOccurred())
		Expect(k8sClient.DeleteAllOf(ctx, &networkv1.Ingress{}, listOpts)).NotTo(HaveOccurred())
		Expect(k8sClient.DeleteAllOf(ctx, &corev1.Secret{}, listOpts)).NotTo(HaveOccurred())

		// delete svc
		svcs := &corev1.ServiceList{}
		Expect(k8sClient.List(ctx, svcs, listOpts)).NotTo(HaveOccurred())
		for _, svc := range svcs.Items {
			Expect(k8sClient.Delete(ctx, &svc, &client.DeleteOptions{})).NotTo(HaveOccurred())
		}

		// delete MachineLearning
		mls := &cksv1beta2.MachineLearningList{}
		Expect(k8sClient.List(ctx, mls, listOpts)).NotTo(HaveOccurred())
		for _, ml := range mls.Items {
			Expect(k8sClient.Delete(ctx, &ml, &client.DeleteOptions{})).NotTo(HaveOccurred())
		}

		mgr, err := ctrl.NewManager(cfg, ctrl.Options{
			Scheme: scheme,
		})
		Expect(err).NotTo(HaveOccurred())

		Expect((&MachineLearningReconciler{
			Client:   k8sClient,
			Scheme:   scheme,
			Recorder: mgr.GetEventRecorderFor("cks-operator"),
		}).SetupWithManager(mgr)).NotTo(HaveOccurred())

		ctx, cancel := context.WithCancel(ctx)
		cancelFunc = cancel
		go func() {
			err = mgr.Start(ctx)
			if err != nil {
				panic(err)
			}
		}()
		time.Sleep(100 * time.Millisecond)
	})
	AfterEach(func() {
		cancelFunc()
		time.Sleep(100 * time.Millisecond)
	})

	It("Create Machinelearning CR with jupyterlab and Delete it", func() {
		// Create NFS PVC
		fakeNfsPvc := newFakeNfsPVC()
		Expect(k8sClient.Create(ctx, fakeNfsPvc, &client.CreateOptions{})).NotTo(HaveOccurred())

		// Update NFS PVC Status
		updatePvcStatusPhase(ctx, fakeNfsPvc)

		// Create CephFS PVC
		fakeCephFsPvc := newFakeCephFsPVC()
		Expect(k8sClient.Create(ctx, fakeCephFsPvc, &client.CreateOptions{})).NotTo(HaveOccurred())

		// Update CephFS PVC Status
		updatePvcStatusPhase(ctx, fakeCephFsPvc)

		// Create MachineLearning CR
		fakeML := newFakeMachineLearning()
		Expect(k8sClient.Create(ctx, fakeML)).Should(Succeed())

		createdML := &cksv1beta2.MachineLearning{}
		Eventually(func() metav1.ConditionStatus {
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(fakeML), createdML)).NotTo(HaveOccurred())
			for _, c := range createdML.Status.Conditions {
				if c.Type == consts.ConditionReady {
					return c.Status
				}
			}
			return ""
		}).Should(Equal(metav1.ConditionTrue))

		ownerReference := metav1.OwnerReference{
			APIVersion:         cksv1beta2.GroupVersion.String(),
			Kind:               "MachineLearning",
			Name:               testMLName,
			UID:                createdML.UID,
			Controller:         pointer.Bool(true),
			BlockOwnerDeletion: pointer.Bool(true),
		}

		dep := &appsv1.Deployment{}
		Expect(k8sClient.Get(ctx, testJupyterClientObjectKey, dep)).Should(Succeed())
		checkOwnerReference(dep, ownerReference)

		cm := &corev1.ConfigMap{}
		Expect(k8sClient.Get(ctx, testJupyterClientObjectKey, cm)).Should(Succeed())
		checkOwnerReference(cm, ownerReference)

		cj := &batchv1.CronJob{}
		Expect(k8sClient.Get(ctx, client.ObjectKey{Name: strings.Join([]string{testJupyterAppName, consts.DiffBackup}, "-"), Namespace: testAppNs}, cj)).Should(Succeed())
		checkOwnerReference(cj, ownerReference)

		ing := &networkv1.Ingress{}
		Expect(k8sClient.Get(ctx, testJupyterClientObjectKey, ing)).Should(Succeed())
		checkOwnerReference(ing, ownerReference)

		secret := &corev1.Secret{}
		Expect(k8sClient.Get(ctx, testJupyterClientObjectKey, secret)).Should(Succeed())
		checkOwnerReference(secret, ownerReference)

		svc := &corev1.Service{}
		Expect(k8sClient.Get(ctx, testJupyterClientObjectKey, svc)).Should(Succeed())
		checkOwnerReference(svc, ownerReference)

		Expect(k8sClient.Delete(ctx, createdML)).NotTo(HaveOccurred())
		Eventually(func() error {
			return k8sClient.Get(ctx, client.ObjectKeyFromObject(createdML), &cksv1beta2.MachineLearning{})
		}).ShouldNot(BeNil())
	})

	It("Create Machinelearning CR with codeserver and Delete it", func() {

		// Create MachineLearning CR
		fakeML := newFakeMachineLearning()
		fakeML.Spec.MachineLearningApps[0].AppName = testCodeServerAppName
		fakeML.Spec.MachineLearningApps[0].MLAppSpec.Type = cksv1beta2.CodeServer
		Expect(k8sClient.Create(ctx, fakeML)).Should(Succeed())

		createdML := &cksv1beta2.MachineLearning{}
		Eventually(func() metav1.ConditionStatus {
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(fakeML), createdML)).NotTo(HaveOccurred())
			for _, c := range createdML.Status.Conditions {
				if c.Type == consts.ConditionReady {
					return c.Status
				}
			}
			return ""
		}).Should(Equal(metav1.ConditionTrue))

		ownerReference := metav1.OwnerReference{
			APIVersion:         cksv1beta2.GroupVersion.String(),
			Kind:               "MachineLearning",
			Name:               testMLName,
			UID:                createdML.UID,
			Controller:         pointer.Bool(true),
			BlockOwnerDeletion: pointer.Bool(true),
		}

		dep := &appsv1.Deployment{}
		Expect(k8sClient.Get(ctx, testCodeServerClientObjectKey, dep)).Should(Succeed())
		checkOwnerReference(dep, ownerReference)

		cm := &corev1.ConfigMap{}
		Expect(k8sClient.Get(ctx, testCodeServerClientObjectKey, cm)).Should(Succeed())
		checkOwnerReference(cm, ownerReference)

		cj := &batchv1.CronJob{}
		Expect(k8sClient.Get(ctx, client.ObjectKey{Name: strings.Join([]string{testCodeServerAppName, consts.DiffBackup}, "-"), Namespace: testAppNs}, cj)).Should(Succeed())
		checkOwnerReference(cj, ownerReference)

		ing := &networkv1.Ingress{}
		Expect(k8sClient.Get(ctx, testCodeServerClientObjectKey, ing)).Should(Succeed())
		checkOwnerReference(ing, ownerReference)

		secret := &corev1.Secret{}
		Expect(k8sClient.Get(ctx, testCodeServerClientObjectKey, secret)).Should(Succeed())
		checkOwnerReference(secret, ownerReference)

		svc := &corev1.Service{}
		Expect(k8sClient.Get(ctx, testCodeServerClientObjectKey, svc)).Should(Succeed())
		checkOwnerReference(svc, ownerReference)

		Expect(k8sClient.Delete(ctx, createdML)).NotTo(HaveOccurred())
		Eventually(func() error {
			return k8sClient.Get(ctx, client.ObjectKeyFromObject(createdML), &cksv1beta2.MachineLearning{})
		}).ShouldNot(Succeed())
	})

})
