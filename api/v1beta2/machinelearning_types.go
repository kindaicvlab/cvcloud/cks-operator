/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta2

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// MachineLearningSpec defines the desired state of MachineLearning
type MachineLearningSpec struct {

	// +kubebuilder:validation:Required
	MachineLearningApps []MachineLearningApps `json:"apps"`
}

// MachineLearningApps is apps for machine learning
type MachineLearningApps struct {

	// +kubebuilder:validation:Required
	AppName string `json:"name"`

	// +kubebuilder:validation:Required
	Namespace string `json:"namespace"`

	// +kubebuilder:validation:Required
	MLAppSpec MLAppSpec `json:"spec"`
}

// MLAppSpec is detail spec of apps for machine learning
type MLAppSpec struct {

	// +kubebuilder:validation:Required
	// +kubebuilder:validation:Enum=jupyterlab;codeserver
	Type ApplicationType `json:"type"`

	// +kubebuilder:validation:Required
	Machine MachineSpec `json:"machine"`

	// +kubebuilder:validation:Required
	AppImage string `json:"appImage"`

	// +optional
	ImagePullSecrets []string `json:"imagePullSecrets,omitempty"`
}

type ApplicationType string

const (
	CodeServer ApplicationType = "codeserver"
	JupyterLab ApplicationType = "jupyterlab"
)

func (t ApplicationType) String() string {
	return string(t)
}

type MachineSpec struct {

	// +kubebuilder:validation:Required
	Type string `json:"type"`

	// +kubebuilder:validation:Required
	Group string `json:"group"`
}

// MachineLearningStatus defines the observed state of MachineLearning
type MachineLearningStatus struct {
	Conditions []metav1.Condition `json:"conditions,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:resource:scope=Namespaced
// +kubebuilder:printcolumn:name="TYPE",type="string",JSONPath=".spec.apps[0].spec.type"
// +kubebuilder:printcolumn:name="MACHINE GROUP",type="string",JSONPath=".spec.apps[0].spec.machine.group"
// +kubebuilder:printcolumn:name="MACHINE TYPE",type="string",JSONPath=".spec.apps[0].spec.machine.type"
// +kubebuilder:printcolumn:name="READY",type="string",JSONPath=".status.conditions[?(@.type=='Ready')].status"
// +kubebuilder:printcolumn:name="IMAGE",type="string",priority=1,JSONPath=".spec.apps[0].spec.appImage"
// +kubebuilder:subresource:status

// MachineLearning is the Schema for the machinelearnings API
type MachineLearning struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   MachineLearningSpec   `json:"spec,omitempty"`
	Status MachineLearningStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// MachineLearningList contains a list of MachineLearning
type MachineLearningList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []MachineLearning `json:"items"`
}

func init() {
	SchemeBuilder.Register(&MachineLearning{}, &MachineLearningList{})
}
