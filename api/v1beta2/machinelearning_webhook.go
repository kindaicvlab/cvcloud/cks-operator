/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta2

import (
	"context"
	"fmt"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"strings"

	imperatorv1alpha1 "github.com/tenzen-y/imperator/pkg/api/v1alpha1"
	"golang.org/x/sync/errgroup"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

var (
	mlLog      = logf.Log.WithName("machinelearning-resource")
	kubeReader client.Reader
	ctx        context.Context
)

func (r *MachineLearning) SetupWebhookWithManager(signalHandler context.Context, mgr ctrl.Manager) error {
	kubeReader = mgr.GetAPIReader()
	ctx = signalHandler
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

var _ webhook.Validator = &MachineLearning{}

// +kubebuilder:webhook:verbs=create,path=/validate-apps-cks-cvlab-v1beta2-machinelearning,mutating=false,failurePolicy=fail,sideEffects=None,groups=apps.cks.cvlab,resources=machinelearnings,versions=v1beta2,name=vmachinelearning.kb.io,admissionReviewVersions={v1,v1beta1}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *MachineLearning) ValidateCreate() error {
	mlLog.Info("validate create", "name", r.Name)
	if err := r.validateAllCondition(); err != nil {
		return err
	}

	return nil
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *MachineLearning) ValidateUpdate(old runtime.Object) error {
	mlLog.Info("validate update", "name", r.Name)
	if err := r.validateAllCondition(); err != nil {
		return err
	}

	return nil
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *MachineLearning) ValidateDelete() error {
	mlLog.Info("validate delete", "name", r.Name)
	if err := r.validateAllCondition(); err != nil {
		return err
	}

	return nil
}

func (r *MachineLearning) validateAllCondition() error {

	eg, ctx := errgroup.WithContext(context.Background())
	for appIndex, app := range r.Spec.MachineLearningApps {

		idx := appIndex
		a := app
		m := app.MLAppSpec.Machine
		eg.Go(func() error {
			select {
			case <-ctx.Done():
				return nil
			default:

				if err := validateAppName(idx, a); err != nil {
					return err
				}
				if err := validateMachineName(idx, m); err != nil {
					return err
				}

				return nil
			}
		})
	}

	if err := eg.Wait(); err != nil {
		return err
	}

	return nil
}

func validateAppName(appIndex int, app MachineLearningApps) error {

	expectedAppName := strings.Join([]string{app.MLAppSpec.Type.String(), app.Namespace}, "-")

	if app.AppName != expectedAppName {
		return field.Invalid(field.NewPath("spec").
			Child("apps").
			Index(appIndex).
			Child("name"),
			app.AppName,
			"Do not match template of appName.")
	}
	return nil
}

func validateMachineName(appIndex int, specifiedMachineSpec MachineSpec) error {
	machine := &imperatorv1alpha1.Machine{}
	if err := kubeReader.Get(ctx, client.ObjectKey{Name: specifiedMachineSpec.Group}, machine); err != nil {
		return err
	}

	availableMachinesMap := make(map[string]imperatorv1alpha1.UsageCondition)
	for _, am := range machine.Status.AvailableMachines {
		availableMachinesMap[am.Name] = am.Usage
	}

	mlLog.Info(fmt.Sprintf("validateMachineName: \n%v\n", availableMachinesMap))

	if _, exist := availableMachinesMap[specifiedMachineSpec.Type]; !exist {
		return field.Invalid(field.NewPath("spec").
			Child("apps").
			Index(appIndex).
			Child("spec").
			Child("machine"),
			specifiedMachineSpec,
			"Failed to find Machine.")
	}

	if availableMachinesMap[specifiedMachineSpec.Type].Reserved == 0 {
		return field.Invalid(field.NewPath("spec").
			Child("apps").
			Index(appIndex).
			Child("spec").
			Child("machine"),
			specifiedMachineSpec,
			"All specified Machine has been used.")
	}

	return nil
}
