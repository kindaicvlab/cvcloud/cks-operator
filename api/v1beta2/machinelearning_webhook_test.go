/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta2

import (
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	imperatorv1alpha1 "github.com/tenzen-y/imperator/pkg/api/v1alpha1"
	imperatorv1alpha1consts "github.com/tenzen-y/imperator/pkg/consts"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	testMachineGroup = "test-machine-group"
	testMLName       = "machinelearning-test"
	testAppName      = "codeserver-test"
	testAppNs        = "test"
)

func newFakeMachineLearning() *MachineLearning {
	return &MachineLearning{
		TypeMeta: metav1.TypeMeta{
			APIVersion: GroupVersion.String(),
			Kind:       "MachineLearning",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      testMLName,
			Namespace: testAppNs,
		},
		Spec: MachineLearningSpec{
			MachineLearningApps: []MachineLearningApps{
				{
					AppName:   testAppName,
					Namespace: testAppNs,
					MLAppSpec: MLAppSpec{
						Type: CodeServer,
						Machine: MachineSpec{
							Group: testMachineGroup,
							Type:  "test-machine1",
						},
						AppImage: "nginx:latest",
					},
				},
			},
		},
	}
}

func newFakeMachine() *imperatorv1alpha1.Machine {
	return &imperatorv1alpha1.Machine{
		TypeMeta: metav1.TypeMeta{
			APIVersion: imperatorv1alpha1.GroupVersion.String(),
			Kind:       imperatorv1alpha1consts.KindMachine,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: testMachineGroup,
			Labels: map[string]string{
				imperatorv1alpha1consts.MachineGroupKey: testMachineGroup,
			},
		},
		Spec: imperatorv1alpha1.MachineSpec{
			NodePool: []imperatorv1alpha1.NodePool{
				{
					Name:  "test-node1",
					Mode:  imperatorv1alpha1.NodeModeReady,
					Taint: false,
					MachineType: []imperatorv1alpha1.NodePoolMachineType{{
						Name: "test-machine1",
					}},
				},
			},
			MachineTypes: []imperatorv1alpha1.MachineType{
				{
					Name: "test-machine1",
					Spec: imperatorv1alpha1.MachineDetailSpec{
						CPU:    resource.MustParse("4"),
						Memory: resource.MustParse("24Gi"),
						GPU: &imperatorv1alpha1.GPUSpec{
							Type:   "nvidia.com/gpu",
							Num:    resource.MustParse("2"),
							Family: "ampere",
						},
					},
					Available: 2,
				},
			},
		},
	}
}

func newFakeMachineUsage() *imperatorv1alpha1.UsageCondition {
	return &imperatorv1alpha1.UsageCondition{
		Maximum:  2,
		Reserved: 2,
		Used:     0,
		Waiting:  0,
	}
}

func updateMachineStatus(ctx context.Context, machine *imperatorv1alpha1.Machine, usage *imperatorv1alpha1.UsageCondition) {
	getMachine := &imperatorv1alpha1.Machine{}
	Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(machine), getMachine)).NotTo(HaveOccurred())
	getMachine.Status = imperatorv1alpha1.MachineStatus{
		AvailableMachines: []imperatorv1alpha1.AvailableMachineCondition{{
			Name:  "test-machine1",
			Usage: *usage,
		}},
	}
	Eventually(func() error {
		return k8sClient.Status().Update(ctx, getMachine, &client.UpdateOptions{})
	}).Should(BeNil())
}

func cleanupEnv() {
	listOpts := client.ListOptions{Namespace: testAppNs}

	// delete MachineLearning
	mls := &MachineLearningList{}
	Expect(k8sClient.List(ctx, mls, &listOpts)).NotTo(HaveOccurred())
	for _, ml := range mls.Items {
		Expect(k8sClient.Delete(ctx, &ml, &client.DeleteOptions{})).NotTo(HaveOccurred())
	}

	Expect(k8sClient.DeleteAllOf(ctx, &imperatorv1alpha1.Machine{}, &client.DeleteAllOfOptions{})).NotTo(HaveOccurred())
}

var _ = Describe("MachineLearning Webhooks", func() {
	BeforeEach(func() {
		cleanupEnv()
	})

	It("Create MachineLearning CR successfully", func() {
		fakeMachine := newFakeMachine()
		Expect(k8sClient.Create(ctx, fakeMachine, &client.CreateOptions{})).NotTo(HaveOccurred())
		updateMachineStatus(ctx, fakeMachine, newFakeMachineUsage())

		fakeML := newFakeMachineLearning()
		Expect(k8sClient.Create(ctx, fakeML, &client.CreateOptions{})).NotTo(HaveOccurred())
	})

	It("Failed to create MachineLearning resource", func() {
		testCases := []struct {
			description  string
			fakeML       *MachineLearning
			machineUsage *imperatorv1alpha1.UsageCondition
			err          bool
		}{
			{
				description:  "Create successfully",
				fakeML:       newFakeMachineLearning(),
				machineUsage: newFakeMachineUsage(),
				err:          false,
			},
			{
				description: "Invalid appName",
				fakeML: func() *MachineLearning {
					ml := newFakeMachineLearning()
					ml.Spec.MachineLearningApps[0].AppName = "invalid"
					return ml
				}(),
				machineUsage: newFakeMachineUsage(),
				err:          true,
			},
			{
				description: "Invalid machine type name",
				fakeML: func() *MachineLearning {
					ml := newFakeMachineLearning()
					ml.Spec.MachineLearningApps[0].MLAppSpec.Machine.Type = "invalid"
					return ml
				}(),
				machineUsage: newFakeMachineUsage(),
				err:          true,
			},
			{
				description: "Invalid machine group name",
				fakeML: func() *MachineLearning {
					ml := newFakeMachineLearning()
					ml.Spec.MachineLearningApps[0].MLAppSpec.Machine.Group = "invalid"
					return ml
				}(),
				machineUsage: newFakeMachineUsage(),
				err:          true,
			},
			{
				description: "All Machine has been used",
				fakeML:      newFakeMachineLearning(),
				machineUsage: func() *imperatorv1alpha1.UsageCondition {
					usage := newFakeMachineUsage()
					usage.Used = 2
					usage.Reserved = 0
					return usage
				}(),
				err: true,
			},
		}

		for _, test := range testCases {
			By(test.description)

			fakeMachine := newFakeMachine()
			Expect(k8sClient.Create(ctx, fakeMachine, &client.CreateOptions{})).NotTo(HaveOccurred())
			updateMachineStatus(ctx, fakeMachine, test.machineUsage)

			err := k8sClient.Create(ctx, test.fakeML, &client.CreateOptions{})
			if test.err {
				Expect(err).To(HaveOccurred())
			} else {
				Expect(err).NotTo(HaveOccurred())
			}
			cleanupEnv()
		}

	})
})
