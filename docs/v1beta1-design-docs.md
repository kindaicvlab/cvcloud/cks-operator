## インスタンスサイズ

- Machine Type

|  Machine Name   | cpu| memory|   GPU  |number of GPU| number of Machine | dependence  |
|:----------------:|:--:|:-----:|:------:|:-----------:|:------------------:|:-----------:|
|    vram-large1   | 12 |  64GB | pascal |      2      |         2          | vram-large2 |
|    vram-large2   | 64 | 300GB | pascal |      4      |         1          | vram-large1 |
|  compute-midium  | 12 |  64GB | turing |      2      |         3          |     N/A     |
|   compute-large  | 20 |  64GB | ampere |      2      |         1          |     N/A     |

- Machine Spec

|            |                           GPU                           |
|:----------:|:-------------------------------------------------------:|
| __pascal__ |                  - Nvidia Quadro GP100                  |
| __turing__ | - Nvidia Geforce RTX2080Ti <br> - Nvidia Quadro RTX8000 |
| __ampere__ |                - Nvidia Geforce RTX3080                 |

## マニフェスト

- Custom Resource

```yaml
apiVersion: apps.cks.cvlab/v1beta1
kind: Machine
metadata:
  name: cks-operator
  labels:
    machineCategory: general 
spec:
  machineTypes:
    - name: vram-large1
      spec:
        cpu: 12
        memory: 64
        gpu: pascal
        gpuNum: 2
        dependence: vram-large2
        available: 2
    - name: vram-large2
      spec:
        cpu: 64
        memory: 300
        gpu: pascal
        gpuNum: 4
        dependence: vram-large1
        available: 1
    - name: compute-midium
      spec:
        cpu: 12
        memory: 64
        gpu: turing
        gpuNum: 2
        dependence: none
        available: 3
    - name: compute-large
      spec:
        cpu: 30
        memory: 64
        gpu: ampere
        gpuNum: 2
        dependence: none
        available: 1
```

```yaml
apiVersion: apps.cks.cvlab/v1beta1
kind: MachineLearning
metadata:
  name: cks-operator
  namespace: iwai
spec:
  apps:
    - name: jupyterlab-iwai
      namespace: iwai
      spec:
        type: jupyterlab
        machine: compute-large
        appImage: registry.gitlab.com/kindaicvlab/deeplearning:cuda10.2-cudnn8
```

```yaml
apiVersion: apps.cks.cvlab/v1beta1
kind: MachineLearning
metadata:
  name: cks-operator
  namespace: habe
spec:
  apps:
    - name: codeserver-habe
      namespace: habe
      spec:
        type: codeserver
        machine: compute-large
        appImage: registry.gitlab.com/kindaicvlab/deeplearning:cuda11.0-cudnn8
```

- MachineStatus

```yaml
status: 
  availableMachines:
    - name: vram-large1
      usage:
        maximum: 2
        used: 1
   - name: vram-large2
      usage:
        maximum: 1
        used: 1
    - name: compute-midium
      usage:
        maximum: 3
        used: 0
    - name: compute-large
      usage:
        maximum: 1
        used: 1
```