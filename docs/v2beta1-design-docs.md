# v2beta1
v2beta1 用のデザインドキュメント

## 概要
cks-operator v1beta1 では，仮想的なマシンを定義し，それを提供することで運用自動化における一定の成果をあげることができた．
しかしながら，半年ほど運用していく中で，仮想リソースの提供意外にも自動化すべきオペレーションが多々見つかったため，v2beta1 として新たな機能を追加する．

## Goal
v2beta1 では，以下のような新たに自動化されるオペレーション及び改善されるオペレーションを実装することである．

- 新規追加
  - コンテナイメージのキャッシュ
    - 機械学習コンテナイメージは一般的にサイズが大きくなり，Pull に時間がかかってしまうため，
    定期的に registry のコンテナイメージ hash と node にダウンロードされているコンテナイメージ hash を比較し，更新されている場合は
    `spec.nodePool` に登録されている node にダミー Pod を起動し，最新のコンテナイメージをダウンロードするようにする．
    node 数が増えるとネットワーク帯域を圧迫して，他のプロセスに影響がでることが考えられるため，同時実行数やスループット制限を設定できるようにする．

## controller の設計
MachineLearning CR 作成後までのシーケンス図は以下のようになっている．

![シーケンス図](./v2beta1-sequence.svg)

### ImageCache controller
- `.spec.downloadSetting` に従って，cronJob を作成する．
- container の中身は何も実行しない．
- [image-cache-controller ディレクトリ](./v2beta1-manifests/image-cache-controller) にあるようなマニフェストを生成する．
- ネットワーク帯域制限に関するドキュメント: https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/network-plugins/

### MachineLearning controller
- [codeserver ディレクトリ](./v2beta1-manifests/codeserver)， [jupyterlab ディレクトリ](./v2beta1-manifests/jupyterlab) ，[job ディレクトリ](./v2beta1-manifests/job)
にあるようなマニフェストを生成する．
- 以下のようにフィールドに対して webhook で検証を行う．
  - 共通:
    - `secret.*`: omitempty
  - codeserver or jupyterlab:
    - `gitSource.*`: omitempty 
  - job:
    - `gitSource.*`: 必須
- job の場合，git レポジトリの指定を必須としているので，args にリポジトリのクローンとクローンしてきたディレクトリへのディレクトリ移動のコマンドを差し込む．

```yaml
secret:
  containerImage: foo //omitempty
  git: bar
gitSource:
  url: https://gitlab.com/kindaicvlab/cvcloud/automl
  branch: main
```

### AutoMachineLearning controller
- [automl ディレクトリ](./v2beta1-manifests/automl) にあるようなマニフェストを生成する．
- `gitSource.*` を必須フィールドとする．
- git レポジトリの指定を必須としているので，args にリポジトリのクローンとクローンしてきたディレクトリへのディレクトリ移動のコマンドを差し込む．

## Custom Resource Schema

### ImageCache リソース

- .metadata.name は owerReference を参照し，`.metadata.name-images` にする．
- spec は machine リソースからそのまま持ってくる．

```yaml
---
apiVersion: apps.cks.cvlab/v2beta1
kind: ImageCache
metadata:
  name: general-machine-images
  labels:
    app.cvcloud.io/name: image-cache
spec:
  downloadSettings:
    concurrent: 1
    bandwidth: 200M
    checkInterval: 900
  registries:
    - type: gitlab
      name: registry.gitlab.com/kindaicvlab/machinelearning-images
      ignoreTags:
        - foo
        - bar
status:
  condition:
    - lastTransitionTime: "2021-07-24T09:08:39Z"
      status: "True"
      type: Ready
  checkRegistryTime: "2021-07-24_09:08:39"
```
### MachineLearning リソース

#### type: jupyterlab & type: codeserver

- v1beta1 とは違い，app を配列ではなく一つだけ受け取れるように変更．
- カスタムイメージ使用を想定して secret.containerImage を受け取るようにする．また，secret.containerImage は `omitempty` で自由記述とする．

```yaml
---
apiVersion: apps.cks.cvlab/v2beta1
kind: MachineLearning
metadata:
  name: jupyterlab-iwai-cks
  namespace: iwai
  labels:
    app.cvcloud.io/name: jupyterlab
    cvcloud.io/machine: general-machine.compute-large
spec:
  type: jupyterlab
  machine: 
    group: general-machine
      name: compute-large
  appImage: registry.gitlab.com/kindaicvlab/machinelearning-images:cuda10.2-cudnn8
  secret:
    # omitempty
    containerImage: foo //omitempty
---
apiVersion: apps.cks.cvlab/v2beta1
kind: MachineLearning
metadata:
  name: jupyterlab-iwai-cks
  namespace: iwai
  labels:
    app.cvcloud.io/name: codeserver
    cvcloud.io/machine: general-machine.compute-midium
spec:
  type: codeserver
  machine:
    group: general-machine          
      name: compute-medium
  appImage: registry.gitlab.com/kindaicvlab/machinelearning-images:cuda10.2-cudnn7
  secret:
    # omitempty
    containerImage: foo //omitempty
```

#### type: job

```yaml
---
apiVersion: apps.cks.cvlab/v2beta1
kind: MachineLearning
metadata:
  name: job-iwai-cks
  namespace: iwai
  labels:
    app.cvcloud.io/name: job
    cvcloud.io/machine: general-machine.compute-large 
spec:
  type: job
  machine:
    group: general-machine
      name: compute-large
  appImage: registry.gitlab.com/kindaicvlab/machinelearning-images:cuda10.2-cudnn8
  command:
    - "python"
    - "train_test.py"
    - "-lr=0.01"
    - "-optimizer=sgd"
  secret:
    # omitempty
    containerImage: foo
    # omitempty
    git: bar
  gitSource:
    url: https://gitlab.com/kindaicvlab/cvcloud/automl
    branch: main
```

### AutoMachineLearning

- machine は group の中から gpu を搭載した物の最も残っている物を使用する．
決定した machine は mutating webhook で `.spec.machine.name` と　`.metadata.labels` に machine name に挿入する．
- parallelTrialCount は `3` を上限にする予定．一つの machineType で `3` を満たせない場合は `2` や `1` に自動で減らす．
- katibExperimentSpec 以外のパラメータはコントローラで自動で埋め込んで Katib experiment を生成する．

```yaml
---
apiVersion: apps.cks.cvlab/v2beta1
kind: AutoMachineLearning
metadata:
  name: automl-iwai-cks
  namespace: iwai
  labels:
    app.cvcloud.io/name: automl
    cvcloud.io/machine: general-machine.xxxxxxxx
spec:
  type: automl
  machine:
    group: general-machine
      name: xxxxxxx
  appImage: registry.gitlab.com/kindaicvlab/machinelearning-images:cuda10.2-cudnn8
  secret:
    containerImage: foo //omitempty
    git: bar
  gitSource:
    url: https://gitlab.com/kindaicvlab/cvcloud/automl
    branch: main
  katibExperimentSpec:
    objective:
      type: maximize
      goal: 0.9
      objectiveMetricName: Validation-accuracy
      additionalMetricNames:
        - loss
    metricsCollectorSpec:
      collector:
        kind: StdOut
    algorithm:
      algorithmName: tpe
    parallelTrialCount: 3
    maxTrialCount: 1000
    maxFailedTrialCount: 200
    parameters:
      - name: learningRate
        parameterType: double
        feasibleSpace:
          min: "0.00009"
          max: "0.0023"
     - name: optimizer
       parameterType: categorical
       feasibleSpace:
         list:
           - sgd
           - adam
    command:
      - "python"
      - "mnist.py"
      - "-lr=${trialParameters.learningRate}"
      - "-optimizer=${trialParameters.optimizer}"
```
