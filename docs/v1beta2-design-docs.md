## インスタンスサイズ

## マニフェスト

- Custom Resource

```yaml
apiVersion: apps.cks.cvlab/v1beta2
kind: MachineLearning
metadata:
  name: cks-operator
  namespace: iwai
spec:
  apps:
    - name: jupyterlab-iwai
      namespace: iwai
      spec:
        type: jupyterlab
        machine: 
          group: general-machine
          type: compute-large
        imagePullSecrets:
          - test-secret
        appImage: ghcr.io/kindaicvlab/machinelearning-images:cuda10.2-cudnn7
```

```yaml
apiVersion: apps.cks.cvlab/v1beta2
kind: MachineLearning
metadata:
  name: cks-operator
  namespace: habe
spec:
  apps:
    - name: codeserver-habe
      namespace: habe
      spec:
        type: codeserver
        machine: 
          group: general-machine
          type: compute-medium
        imagePullSecrets:
          - test-secret
        appImage: ghcr.io/kindaicvlab/machinelearning-images:cuda10.2-cudnn7
```
