#!/usr/bin/env bash
set -eox pipefail
cd "$(dirname "$0")"
TIMEOUT=5m

# Setup cks-operator
echo "Setup cks-operator"
yq eval -i '.spec.template.spec.containers[0].imagePullPolicy|="IfNotPresent"' ../config/manager/manager.yaml
yq eval -i '.images[0].newTag|="latest"' ../config/manager/kustomization.yaml

kustomize build ../config/crd | kubectl apply -f -
kustomize build ../config/default | kubectl apply -f -
kubectl wait pods -n cks-system --for condition=ready --timeout=${TIMEOUT} -l app.kubernetes.io/name=cks-operator
kubectl apply -f ../example/machinelearning.yaml

yq eval -i '.spec.template.spec.containers[0].imagePullPolicy|="Always"' ../config/manager/manager.yaml
