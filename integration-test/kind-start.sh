# stolen from https://github.com/tenzen-y/imperator/blob/0139f28bc494e7355975a683924da4efa166ed60/hack/kind-start.sh and modified.
# Copyright 2021 Yuki Iwai (@tenzen-y)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#!/usr/bin/env bash
set -eox pipefail
cd "$(dirname "$0")"

KIND_CONFIG=${1:-./kind-config.yaml}

kind create cluster --config ${KIND_CONFIG}
kubectl config use-context kind-kind

# Wait for Start Kind Node
TIMEOUT=5m
kubectl wait --for condition=ready --timeout=${TIMEOUT} node kind-control-plane
kubectl taint nodes --all node-role.kubernetes.io/master- || true
kubectl taint nodes --all  node-role.kubernetes.io/control-plane- || true

# Deploy CertManager
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/latest/download/cert-manager.yaml
kubectl wait pods -n cert-manager --for condition=ready --timeout=${TIMEOUT} -l "app.kubernetes.io/name in (webhook,cainjector,cert-manager)"

# Deploy Imperator
kubectl apply -f https://raw.githubusercontent.com/tenzen-y/imperator/v0.0.2/deploy/imperator.yaml
kubectl wait pods -n imperator-system --for condition=ready --timeout=${TIMEOUT} -l app.kubernetes.io/name=imperator

# Deploy Machine
kubectl apply -f https://raw.githubusercontent.com/tenzen-y/imperator/v0.0.2/examples/machine/general-machine.yaml
kubectl get machines.imperator.tenzen-y.io,machinenodepools.imperator.tenzen-y.io
kubectl get pods -n imperator-system
kubectl describe machines general-machine

# Prepare
kustomize build ./manifests | kubectl apply -f -
kubectl wait pods hold-volume --for condition=ready -n utaha
kubectl get pods -n utaha
