[![Pipeline Status](https://gitlab.com/kindaicvlab/cvcloud/cks-operator/badges/master/pipeline.svg)](https://gitlab.com/kindaicvlab/cvcloud/cks-operator/-/pipelines)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/kindaicvlab/cvcloud/cks-operator)](https://goreportcard.com/report/gitlab.com/kindaicvlab/cvcloud/cks-operator)
[![Coverage](https://gitlab.com/kindaicvlab/cvcloud/cks-operator/badges/master/coverage.svg)](https://gitlab.com/kindaicvlab/cvcloud/cks-operator/-/commits/master)

# cks-operator

The `cks-operator` provides CVLAB members with a fully managed "JupyterLab" or "CodeServer".
