/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package consts

const (
	// DiffBackup is app type for diffbackup
	DiffBackup = "diffbackup"
	// ConditionReady is mean that apps ready
	ConditionReady = "Ready"
	// BackUpConfigMapName is configMap name for cronjob to run backup
	BackUpConfigMapName = "backup-config"
	// BackUpImage is container image name for cronjob to run backup
	BackUpImage = "registry.gitlab.com/kindaicvlab/cvcloud/backup:latest"
	// IngressClassName is ingressClass for IDE
	IngressClassName = "ml.apps.cks"
	// IngressHostName is host name for ingress
	IngressHostName = "cvcloud.cks.local"

	// CephFsLabel is label's element for persistent volume claim provided by cks-operator to create cephfs volumes
	CephFsLabel = "cephfs"
	// NfsLabel is label's element for persistent volume claim provided by cks-operator to create nfs volumes
	NfsLabel = "nfs"

	CksAppTypeLabelKey     = "cvcloud.cks.cvlab/app-type"
	CksAppNameLabelKey     = "cvcloud.cks.cvlab/app-name"
	CksStorageTypeLabelKey = "cvcloud.cks.cvlab/storage-type"

	RunAsUser int64 = 1000
)
